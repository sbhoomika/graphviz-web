FROM golang
ADD . /app
WORKDIR /app
RUN go build web.go
CMD ["/app/web"]
